

// cau 1
function inSo(){
    var contentRow = '';
    var contentHTML = '';
    
    var n = 0;
    for(var i = 0; i < 10; i++) {
        contentRow = ' ';
        for(var j = 0; j < 10; j++) {
            n++;
            var content = `<div class='col-1-2'><span>${n}</span></div>`;
            contentRow += content;
        }
        contentHTML += `<div class='row'>${contentRow}</div>` 
    }
    document.getElementById('result-one').innerHTML = `<div class='col-12'>${contentHTML}</div>`;

}

// cau 2
var arrCau2 = [];
function taoArrCau2(){
    var value = document.getElementById('new-numberCau2').value*1;
    if(Number.isInteger(value)){
        arrCau2.push(value);
        document.getElementById('result-arrCau2').innerHTML = `<h5>${arrCau2}</h5>`;
    }
    else {
        alert('Chỉ nhập số nguyên!')
    }
}

function kiemTraSoNguyenTo(n){
    for(var i = 2; i <= Math.sqrt(n); i++){
        if(n % i == 0){
            return false;
        }
    }
    return true;
}
function timSoNguyenToCau2() {
    var newArrCau2 = [];
    for(var i = 0; i < arrCau2.length; i++) {
        var item = arrCau2[i];
        if(item > 1) {
            if(kiemTraSoNguyenTo(item) == true) {
                newArrCau2.push(item);
            }
        }
    }
    if(newArrCau2.length == 0) {
        document.getElementById('result-two').innerHTML = 'mảng không có sô nguyên tố';
    }
    else {
        document.getElementById('result-two').innerHTML = `<h5>${newArrCau2}</h5>`;
    }
}



// cau 3
function tinhTongCau3(){
    var n = document.getElementById('new-numberCau3').value*1;
    var sum = 0;
    if(Number.isInteger(n)) {
        for(var i = 2; i <= n; i++) {
            sum += i;
        }
        sum += 2*n;
        document.getElementById('result-three').innerHTML = `<h5>Tổng : ${sum}</h5>`
    }
    else {
        alert('n phải là số nguyên!')
    }
}

// cau 4
function tinhSoLuongUoc(){
    var n = document.getElementById('new-numberCau4').value*1;
    var arrUoc = [];
    if(Number.isInteger(n) && n > 0) {
        for(var i = 0; i <= n; i++) {
            if(n % i == 0) {
                arrUoc.push(i);
            }
        }
        document.getElementById('result-four').innerHTML = `<h5>Số lượng ước là: ${arrUoc.length}</h5>
                                                            <h5> Ước là: ${arrUoc}</h5>`
    }
    else {
        alert('n phải là số nguyên dương!')
    }
}

// cau 5
function daoNguocSo(){
    var n = document.getElementById('new-numberCau5').value*1;
    
    var newNumber = '';
    var x = String(n);
    
    if(Number.isInteger(n) && n >0) {
        for(var i = x.length -1; i >= 0; i--) {
            newNumber += x[i];
        }
        document.getElementById('result-five').innerHTML = `<h5>Số đảo ngược: ${newNumber}</h5>`
    }
    else {
        alert('n phải là số nguyên dương!');
    }
}

// cau 6
function timfNguyenDuongLonNhat(){
    var sum = 0;
    var i = 0;
    while(sum < 100) {
        i++;
        sum += i;
    }
    document.getElementById('result-six').innerHTML = `<h5>X = ${i-1}</h5>`
}

// cau 7
function inBangCuuChuong(){
    var n = document.getElementById('new-numberCau7').value*1;
    var contentHtml = '';
    for(var i = 0; i <= 10; i++) {
        var content = `<h5>${n} x ${i} = ${n*i}</h5>`;
        contentHtml += content
    }
    document.getElementById('result-seven').innerHTML = contentHtml;
}

// câu 8
function chiaBai(){
    var players = [ [], [], [], []];
    var cards = ["4K", "KH", "5C", "KA", "QH", "KD", "2H", "10S", "AS", "7H", "9K", "10D"];
    var i = 0;
    while(i < cards.length) {
        for(var j = 0; j < players.length; j++) {
            players[j].push(cards[i]);
            i++;
            if( i >= cards.length) {
                break;
            }
        }
    }
    var contentHTML = '';
    for(var i = 0; i < players.length; i++) {
        var content = `<h5>Player ${i+1} = [${players[i]}]</h5>`
        contentHTML += content;
    }
    document.getElementById('result-eight').innerHTML = contentHTML;
}

// cau 9

function timGaCho(){
    var soCon = document.getElementById('soCon').value*1;
    var soChan = document.getElementById('soChan').value*1;
    var soChanga = soCon * 2;
    var chanThua = soChan - soChanga;
    var soCho = chanThua / 2;
    var soGa = soCon - soCho;
    console.log(soChan%2==0)
    if(Number.isInteger(soCon) && soChan%2 ==0) {
        if(soGa > 0 && soCho > 0) {
            document.getElementById('result-nine').innerHTML = 
            `<h5>Số gà: ${soGa}, Số gà: ${soCho}</h5>`
        }
        else {
            alert('Số con và số chân không tương ứng!')
        }
    }
    else {
        alert('Số con phải là số nguyên, số chân phải là số chẫn!')
    }
    
}
// cau 10
function tinhGoc(){
    var soGio = document.getElementById('soGio').value*1;
    var soPhut = document.getElementById('soPhut').value*1;
    var temp = false;
    if(soGio < 12 && soGio >=0 && soPhut < 60 && soPhut >= 0
        && Number.isInteger(soGio) && Number.isInteger(soPhut)) {
        temp = true;
    }
    console.log("🚀 ~ file: index.js:193 ~ tinhGoc ~ temp", temp)
    // 1 giờ kim giờ quay đc 30 độ
    // 1 phút kim phút quay đc 6 độ
    if(temp) {
        var gocKimGio = soGio*30 + ((soPhut/60)*30)
        var gocKimPhut = soPhut*6;
        var goc2Kim = gocKimPhut-gocKimGio
        document.getElementById('result-ten').innerHTML = 
        `<h5>Góc giữa 2 kim là: ${Math.abs(goc2Kim)} độ</h5>`
    }
    else {
        document.getElementById('result-ten').innerHTML = 
        `<h5>Số giờ phải lơn hơn 0 và nhỏ hơn 12</h5>
        <h5>Số phút phải lơn hơn 0 và nhỏ hơn 60</h5>
        <h5>Số giờ và phút phải là số nguyên</h5>`
    }
}       